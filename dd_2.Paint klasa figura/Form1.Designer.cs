﻿namespace dd_2.Paint_klasa_figura
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButtonProsta = new System.Windows.Forms.RadioButton();
            this.radioButtonElipsa = new System.Windows.Forms.RadioButton();
            this.radioButtonKrzywa = new System.Windows.Forms.RadioButton();
            this.radioButtonProstakat = new System.Windows.Forms.RadioButton();
            this.pictureBoxRysunek = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBoxCzyWypelnienie = new System.Windows.Forms.CheckBox();
            this.buttonCzysc = new System.Windows.Forms.Button();
            this.buttonZew = new System.Windows.Forms.Button();
            this.buttonWew = new System.Windows.Forms.Button();
            this.colorDialogZew = new System.Windows.Forms.ColorDialog();
            this.colorDialogWew = new System.Windows.Forms.ColorDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRysunek)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // radioButtonProsta
            // 
            this.radioButtonProsta.AutoSize = true;
            this.radioButtonProsta.Location = new System.Drawing.Point(6, 22);
            this.radioButtonProsta.Name = "radioButtonProsta";
            this.radioButtonProsta.Size = new System.Drawing.Size(55, 17);
            this.radioButtonProsta.TabIndex = 0;
            this.radioButtonProsta.TabStop = true;
            this.radioButtonProsta.Text = "Prosta";
            this.radioButtonProsta.UseVisualStyleBackColor = true;
            // 
            // radioButtonElipsa
            // 
            this.radioButtonElipsa.AutoSize = true;
            this.radioButtonElipsa.Location = new System.Drawing.Point(6, 68);
            this.radioButtonElipsa.Name = "radioButtonElipsa";
            this.radioButtonElipsa.Size = new System.Drawing.Size(53, 17);
            this.radioButtonElipsa.TabIndex = 1;
            this.radioButtonElipsa.TabStop = true;
            this.radioButtonElipsa.Text = "Elipsa";
            this.radioButtonElipsa.UseVisualStyleBackColor = true;
            // 
            // radioButtonKrzywa
            // 
            this.radioButtonKrzywa.AutoSize = true;
            this.radioButtonKrzywa.Location = new System.Drawing.Point(6, 45);
            this.radioButtonKrzywa.Name = "radioButtonKrzywa";
            this.radioButtonKrzywa.Size = new System.Drawing.Size(59, 17);
            this.radioButtonKrzywa.TabIndex = 2;
            this.radioButtonKrzywa.TabStop = true;
            this.radioButtonKrzywa.Text = "Kryzwa";
            this.radioButtonKrzywa.UseVisualStyleBackColor = true;
            // 
            // radioButtonProstakat
            // 
            this.radioButtonProstakat.AutoSize = true;
            this.radioButtonProstakat.Location = new System.Drawing.Point(6, 91);
            this.radioButtonProstakat.Name = "radioButtonProstakat";
            this.radioButtonProstakat.Size = new System.Drawing.Size(70, 17);
            this.radioButtonProstakat.TabIndex = 3;
            this.radioButtonProstakat.TabStop = true;
            this.radioButtonProstakat.Text = "Prostokąt";
            this.radioButtonProstakat.UseVisualStyleBackColor = true;
            // 
            // pictureBoxRysunek
            // 
            this.pictureBoxRysunek.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxRysunek.Name = "pictureBoxRysunek";
            this.pictureBoxRysunek.Size = new System.Drawing.Size(440, 321);
            this.pictureBoxRysunek.TabIndex = 4;
            this.pictureBoxRysunek.TabStop = false;
            this.pictureBoxRysunek.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxRysunek_MouseDown);
            this.pictureBoxRysunek.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBoxRysunek_MouseMove);
            this.pictureBoxRysunek.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxRysunek_MouseUp);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonProstakat);
            this.groupBox1.Controls.Add(this.radioButtonProsta);
            this.groupBox1.Controls.Add(this.radioButtonElipsa);
            this.groupBox1.Controls.Add(this.radioButtonKrzywa);
            this.groupBox1.Location = new System.Drawing.Point(458, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 131);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Figury";
            // 
            // checkBoxCzyWypelnienie
            // 
            this.checkBoxCzyWypelnienie.AutoSize = true;
            this.checkBoxCzyWypelnienie.Location = new System.Drawing.Point(464, 160);
            this.checkBoxCzyWypelnienie.Name = "checkBoxCzyWypelnienie";
            this.checkBoxCzyWypelnienie.Size = new System.Drawing.Size(101, 17);
            this.checkBoxCzyWypelnienie.TabIndex = 7;
            this.checkBoxCzyWypelnienie.Text = "Czy wypelnienie";
            this.checkBoxCzyWypelnienie.UseVisualStyleBackColor = true;
            // 
            // buttonCzysc
            // 
            this.buttonCzysc.Location = new System.Drawing.Point(464, 183);
            this.buttonCzysc.Name = "buttonCzysc";
            this.buttonCzysc.Size = new System.Drawing.Size(75, 23);
            this.buttonCzysc.TabIndex = 8;
            this.buttonCzysc.Text = "Czyść";
            this.buttonCzysc.UseVisualStyleBackColor = true;
            this.buttonCzysc.Click += new System.EventHandler(this.buttonCzysc_Click);
            // 
            // buttonZew
            // 
            this.buttonZew.Location = new System.Drawing.Point(464, 238);
            this.buttonZew.Name = "buttonZew";
            this.buttonZew.Size = new System.Drawing.Size(75, 95);
            this.buttonZew.TabIndex = 9;
            this.buttonZew.UseVisualStyleBackColor = true;
            this.buttonZew.Click += new System.EventHandler(this.buttonZew_Click);
            // 
            // buttonWew
            // 
            this.buttonWew.Location = new System.Drawing.Point(580, 238);
            this.buttonWew.Name = "buttonWew";
            this.buttonWew.Size = new System.Drawing.Size(75, 95);
            this.buttonWew.TabIndex = 10;
            this.buttonWew.UseVisualStyleBackColor = true;
            this.buttonWew.Click += new System.EventHandler(this.buttonWew_Click);
            // 
            // colorDialogWew
            // 
            this.colorDialogWew.Color = System.Drawing.Color.DarkRed;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(461, 222);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Kolor linii:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(577, 222);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Kolor wypełnienia:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 345);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonWew);
            this.Controls.Add(this.buttonZew);
            this.Controls.Add(this.buttonCzysc);
            this.Controls.Add(this.checkBoxCzyWypelnienie);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBoxRysunek);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRysunek)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonProsta;
        private System.Windows.Forms.RadioButton radioButtonElipsa;
        private System.Windows.Forms.RadioButton radioButtonKrzywa;
        private System.Windows.Forms.RadioButton radioButtonProstakat;
        private System.Windows.Forms.PictureBox pictureBoxRysunek;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBoxCzyWypelnienie;
        private System.Windows.Forms.Button buttonCzysc;
        private System.Windows.Forms.Button buttonZew;
        private System.Windows.Forms.Button buttonWew;
        private System.Windows.Forms.ColorDialog colorDialogZew;
        private System.Windows.Forms.ColorDialog colorDialogWew;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

