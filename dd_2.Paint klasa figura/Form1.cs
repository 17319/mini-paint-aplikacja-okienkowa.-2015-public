﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dd_2.Paint_klasa_figura
{
    public partial class Form1 : Form
    {
        Graphics g;
        List<Point> punkty;
        List<Figura> narysowane;
        public Form1()
        {
            InitializeComponent();
            buttonZew.BackColor = colorDialogZew.Color;
            buttonWew.BackColor = colorDialogWew.Color;
            pictureBoxRysunek.Image = new Bitmap(pictureBoxRysunek.Width, pictureBoxRysunek.Height);
            g = Graphics.FromImage(pictureBoxRysunek.Image);
            g.Clear(Color.Wheat);
            punkty = new List<Point>();
            narysowane = new List<Figura>();
 
        }

        private void pictureBoxRysunek_MouseDown(object sender, MouseEventArgs e)
        {
            punkty.Clear();
            punkty.Add(e.Location);
        }

        private void pictureBoxRysunek_MouseMove(object sender, MouseEventArgs e)
        {
            punkty.Add(e.Location);
        }

        private void pictureBoxRysunek_MouseUp(object sender, MouseEventArgs e)
        {
            bool czyWypelnienie;
            if (checkBoxCzyWypelnienie.Checked) czyWypelnienie = true;
            else czyWypelnienie = false;
            punkty.Add(e.Location);
            if (radioButtonKrzywa.Checked)
            {
                narysowane.Add(new Krzywa(5, colorDialogZew.Color, punkty));
            }
            else if (radioButtonElipsa.Checked)
            {
                narysowane.Add(new Elipsa(5, colorDialogZew.Color, colorDialogWew.Color, czyWypelnienie, punkty.First(),
                    punkty.Last().X-punkty.First().X, punkty.Last().Y-punkty.First().Y));
            }
            else if (radioButtonProsta.Checked)
            {
                narysowane.Add(new Prosta(5,colorDialogZew.Color,punkty));
            }
            else if (radioButtonProstakat.Checked)
            {
                narysowane.Add(new Prostakat(5, colorDialogZew.Color, colorDialogWew.Color, czyWypelnienie, new Point(Math.Min(punkty.First().X, punkty.Last().X), Math.Min(punkty.First().Y, punkty.Last().Y)),
                    Math.Abs(punkty.Last().X - punkty.First().X), Math.Abs(punkty.Last().Y - punkty.First().Y)));
            }
            else
            {
                MessageBox.Show("Nie wybrano opcji");
            }
            if (narysowane.Count!=0)
            narysowane.Last().Rysuj(g);
            pictureBoxRysunek.Refresh();
        }

        private void buttonCzysc_Click(object sender, EventArgs e)
        {
            g.Clear(Color.Wheat);
            pictureBoxRysunek.Refresh();
        }

        private void buttonZew_Click(object sender, EventArgs e)
        {
            colorDialogZew.ShowDialog();
            buttonZew.BackColor = colorDialogZew.Color;
            buttonZew.Refresh();
        }

        private void buttonWew_Click(object sender, EventArgs e)
        {
            colorDialogWew.ShowDialog();
            buttonWew.BackColor = colorDialogWew.Color;
            buttonWew.Refresh();
        }
    }
}
