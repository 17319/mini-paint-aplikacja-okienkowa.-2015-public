﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dd_2.Paint_klasa_figura
{
    abstract class Figura
    {
        protected int liniaGrubosc;
        protected Color kolorLinii;
        protected Color kolorWypelnienia;
        protected Figura(int liniaGrubosc, Color liniaKolor, Color kolorWypelnienia)
        {
            this.liniaGrubosc = liniaGrubosc;
            this.kolorLinii = liniaKolor;
            this.kolorWypelnienia = kolorWypelnienia;
        }
        abstract public void Rysuj(Graphics g);
    }
    abstract class FiguraWypelniona : Figura
    {
        protected Color wypelnienieKolor;
        protected Boolean czyWypelnienie;
        protected FiguraWypelniona(int liniaGrubosc, Color liniaKolor, Color wypelnienieKolor,
            Boolean czyWypelnienie)
            : base(liniaGrubosc, liniaKolor,wypelnienieKolor)
        {
            this.wypelnienieKolor = wypelnienieKolor;
            this.czyWypelnienie = czyWypelnienie;
        }
    }

    sealed class Elipsa : FiguraWypelniona
    {
        private Point naroznik;
        private int szerokosc, wysokosc;
        public Elipsa(int liniaGrubosc, Color liniaKolor, Color wypelnienieKolor,
            Boolean czyWypelnienie, Point naroznik, int szerokosc, int wysokosc)
            : base(liniaGrubosc, liniaKolor, wypelnienieKolor, czyWypelnienie)
        {
            this.naroznik = naroznik;
            this.szerokosc = szerokosc;
            this.wysokosc = wysokosc;
        }
        public override void Rysuj(Graphics g)
        {
            Pen p = new Pen(kolorLinii, liniaGrubosc);
            if (czyWypelnienie)
                g.FillEllipse(new SolidBrush(kolorWypelnienia), naroznik.X, naroznik.Y, szerokosc, wysokosc);
            g.DrawEllipse(p, naroznik.X, naroznik.Y, szerokosc, wysokosc);
        }
    }

    sealed class Prostakat : FiguraWypelniona
    {
        private Point naroznik;
        private int szerokosc, wysokosc;
        public Prostakat(int liniaGrubosc, Color liniaKolor, Color wypelnienieKolor,
            Boolean czyWypelnienie, Point naroznik, int szerokosc, int wysokosc)
            : base(liniaGrubosc, liniaKolor, wypelnienieKolor, czyWypelnienie)
        {
            this.naroznik = naroznik;
            this.szerokosc = szerokosc;
            this.wysokosc = wysokosc;
        }
        public override void Rysuj(Graphics g)
        {
            Pen p = new Pen(kolorLinii, liniaGrubosc);

            if (czyWypelnienie)
                g.FillRectangle(new SolidBrush(kolorWypelnienia), naroznik.X, naroznik.Y, szerokosc, wysokosc);
            g.DrawRectangle(p, naroznik.X, naroznik.Y, szerokosc, wysokosc);
        }
    }

    sealed class Krzywa : Figura
    {
        private List<Point> punkty;
        public Krzywa(int liniaGrubosc, Color liniaKolor, List<Point> punkty)
            : base(liniaGrubosc, liniaKolor,liniaKolor)
        {
            this.punkty = punkty;
        }
        public override void Rysuj(Graphics g)
        {
            Pen pen = new Pen(kolorLinii, liniaGrubosc);
            for (int i=0;i<punkty.Count()-1;i++)
                g.DrawLine(pen, punkty[i].X, punkty[i].Y, punkty[i+1].X, punkty[i+1].Y);
        }
    }

    sealed class Prosta : Figura
    {
        private List<Point> punkty;
        public Prosta(int liniaGrubosc, Color liniaKolor, List<Point> punkty)
            : base(liniaGrubosc, liniaKolor, liniaKolor)
        {
            this.punkty = punkty;
        }
        public override void Rysuj(Graphics g)
        {
            Pen pen = new Pen(kolorLinii, liniaGrubosc);
            for (int i = 0; i < punkty.Count() - 1; i++)
                g.DrawLine(pen, punkty.First(), punkty.Last());
        }
    }


}
